<?php

namespace App\Repository;

use App\Entity\Ficha;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Ficha>
 *
 * @method Ficha|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ficha|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ficha[]    findAll()
 * @method Ficha[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FichaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ficha::class);
    }

    public function save(Ficha $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Ficha $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function getFichaVigilante(): ?array
    {
        //se hace un left join para obtener las fichas aun cuando no tengan registrado un medico Id
        $strSql = "SELECT fichas.id,
                   fichas.descripcion,
                   fichas.delitos,
                   fichas.sentencia,
                   fichas.nombres,
                   fichas.apellidos,
                   fichas.estado_ficha,
                   fichas.estado_preso,
                   fichas.fecha_salida,
                   fichas.fecha_entrada,
                   celdas.id numero, 
                   tipoTurno.precio valorTurno,
                   userVigilante.nombres nombre_medico,
                   userVigilante.apellidos apellido_medico,
                   FROM App\Entity\Ficha fichas
                   LEFT JOIN App\Entity\Usuario userVigilante
                   WITH fichas.vigilante_id = userVigilante.id
                   LEFT JOIN App\Entity\Celda celdas
                   WITH fichas.celda_id = celdas.id
                   WHERE fichas.estado_ficha =:estado1 AND fichas.estado_ficha =:estado2 AND fichas.estado_base = :estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado1','En Espera')
                    ->setParameter('estado2','Rechazada')
                    ->setParameter('estado',"A")
                    ->getResult();         
    }
    public function getFichaAdmin(): ?array
    {
        //se hace un left join para obtener las fichas aun cuando no tengan registrado un medico Id
        $strSql = "SELECT fichas.id,
                   fichas.descripcion,
                   fichas.delitos,
                   fichas.sentencia,
                   fichas.nombres,
                   fichas.apellidos,
                   fichas.estado_ficha,
                   fichas.estado_preso,
                   fichas.fecha_salida,
                   fichas.fecha_entrada, 
                   tipoTurno.precio valorTurno,
                   userVigilante.nombres nombre_medico,
                   userVigilante.apellidos apellido_medico,
                   FROM App\Entity\Ficha fichas
                   LEFT JOIN App\Entity\Usuario userVigilante
                   WITH fichas.vigilante_id = userVigilante.id
                   WHERE fichas.estado_ficha =:estado1 AND fichas.estado_base = :estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado1','En Espera')
                    
                    ->setParameter('estado',"A")
                    ->getResult();         
    }

//    /**
//     * @return Ficha[] Returns an array of Ficha objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Ficha
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
