<?php

namespace App\Form;

use App\Entity\Ficha;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterFichaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        
          if($options['accion']=='crearFicha')
          {
            $builder
            ->add('descripcion',TextType::class)
            ->add('delitos',TextType::class)
            ->add('sentencia',TextType::class)
            ->add('nombres',TextType ::class)
            ->add('apellidos',TextType::class);
          }
            
            //->add('estado_ficha')
            //->add('estado_preso')
            //->add('vigilante_id')
            //->add('estado_base')
            //->add('celda_id',)
            if($options['accion']=='asignarCelda')
            {
              $builder
                ->add('celda_id',ChoiceType::class,
                ['choices'=>$options["celdas"],//array que se envia desde el controlador
                 'choice_value'=>'id',//atributo de valor que se setea al seleccionar una opcion
                 'choice_label'=>'descripcion',//lo que se mostrara en pantala en las opciones
                 'label'=>'Tipo',//lo que se muestra en pantalla por defecto
                 'mapped'=>false]) 
            ->add('fecha_salida',DateType::class)
            ->add('fecha_entrada',DateType::class) ; 
            }
            
         $builder ->add('save',SubmitType::class, ['label' => 'Registrar']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ficha::class,
            'accion'=> 'crearFicha',
            'celdas'=>array()
        ]);
    }
}
