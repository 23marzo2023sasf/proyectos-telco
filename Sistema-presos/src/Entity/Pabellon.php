<?php

namespace App\Entity;

use App\EntityUsuario;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Pabellon
 *
 * @ORM\Table(name="pabellon", indexes={@ORM\Index(name="FK_vigilante_pabellon", columns={"vigilante_id"})})
 * @ORM\Entity
 */
class Pabellon
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=false)
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\Column(name="capacidad", type="integer", nullable=false)
     */
    private $capacidad;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=25, nullable=false)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_base", type="string", length=1, nullable=false)
     */
    private $estadoBase;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vigilante_id", referencedColumnName="id")
     * })
     */
    private $vigilante;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getCapacidad(): ?int
    {
        return $this->capacidad;
    }

    public function setCapacidad(int $capacidad): self
    {
        $this->capacidad = $capacidad;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getEstadoBase(): ?string
    {
        return $this->estadoBase;
    }

    public function setEstadoBase(string $estadoBase): self
    {
        $this->estadoBase = $estadoBase;

        return $this;
    }

    public function getVigilante(): ?Usuario
    {
        return $this->vigilante;
    }

    public function setVigilante(?Usuario $vigilante): self
    {
        $this->vigilante = $vigilante;

        return $this;
    }


}
