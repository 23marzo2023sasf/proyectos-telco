<?php

namespace App\Entity;

use App\Entity\Celda;
use App\Entity\Usuario;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Ficha
 *
 * @ORM\Table(name="ficha", indexes={@ORM\Index(name="FK_celda_ficha", columns={"celda_id"}), @ORM\Index(name="FK_vigilante_ficha", columns={"vigilante_id"})})
 * @ORM\Entity
 */
class Ficha
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=false)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="delitos", type="string", length=100, nullable=false)
     */
    private $delitos;

    /**
     * @var string
     *
     * @ORM\Column(name="sentencia", type="string", length=100, nullable=false)
     */
    private $sentencia;

    /**
     * @var string
     *
     * @ORM\Column(name="nombres", type="string", length=50, nullable=false)
     */
    private $nombres;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=50, nullable=false)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_ficha", type="string", length=25, nullable=false)
     */
    private $estadoFicha;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_preso", type="string", length=25, nullable=false)
     */
    private $estadoPreso;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_base", type="string", length=1, nullable=false)
     */
    private $estadoBase;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_entrada", type="date", nullable=true, options={"default"="NULL"})
     */
    private $fechaEntrada = 'NULL';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_salida", type="date", nullable=true, options={"default"="NULL"})
     */
    private $fechaSalida = 'NULL';

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vigilante_id", referencedColumnName="id")
     * })
     */
    private $vigilante;

    /**
     * @var Celda
     *
     * @ORM\ManyToOne(targetEntity="Celda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="celda_id", referencedColumnName="id")
     * })
     */
    private $celda;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getDelitos(): ?string
    {
        return $this->delitos;
    }

    public function setDelitos(string $delitos): self
    {
        $this->delitos = $delitos;

        return $this;
    }

    public function getSentencia(): ?string
    {
        return $this->sentencia;
    }

    public function setSentencia(string $sentencia): self
    {
        $this->sentencia = $sentencia;

        return $this;
    }

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(string $nombres): self
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getEstadoFicha(): ?string
    {
        return $this->estadoFicha;
    }

    public function setEstadoFicha(string $estadoFicha): self
    {
        $this->estadoFicha = $estadoFicha;

        return $this;
    }

    public function getEstadoPreso(): ?string
    {
        return $this->estadoPreso;
    }

    public function setEstadoPreso(string $estadoPreso): self
    {
        $this->estadoPreso = $estadoPreso;

        return $this;
    }

    public function getEstadoBase(): ?string
    {
        return $this->estadoBase;
    }

    public function setEstadoBase(string $estadoBase): self
    {
        $this->estadoBase = $estadoBase;

        return $this;
    }

    public function getFechaEntrada(): ?\DateTimeInterface
    {
        return $this->fechaEntrada;
    }

    public function setFechaEntrada(?\DateTimeInterface $fechaEntrada): self
    {
        $this->fechaEntrada = $fechaEntrada;

        return $this;
    }

    public function getFechaSalida(): ?\DateTimeInterface
    {
        return $this->fechaSalida;
    }

    public function setFechaSalida(?\DateTimeInterface $fechaSalida): self
    {
        $this->fechaSalida = $fechaSalida;

        return $this;
    }

    public function getVigilante(): ?Usuario
    {
        return $this->vigilante;
    }

    public function setVigilante(?Usuario $vigilante): self
    {
        $this->vigilante = $vigilante;

        return $this;
    }

    public function getCelda(): ?Celda
    {
        return $this->celda;
    }

    public function setCelda(?Celda $celda): self
    {
        $this->celda = $celda;

        return $this;
    }


}
