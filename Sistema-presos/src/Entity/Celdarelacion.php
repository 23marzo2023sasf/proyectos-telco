<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Celdarelacion
 *
 * @ORM\Table(name="celdarelacion")
 * @ORM\Entity
 */
class Celdarelacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="celda_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $celdaId;

    /**
     * @var int
     *
     * @ORM\Column(name="guardia_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $guardiaId;

    public function getCeldaId(): ?int
    {
        return $this->celdaId;
    }

    public function getGuardiaId(): ?int
    {
        return $this->guardiaId;
    }


}
