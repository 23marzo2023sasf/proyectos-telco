<?php

namespace App\Entity;

use App\Entity\Pabellon;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Celda
 *
 * @ORM\Table(name="celda", indexes={@ORM\Index(name="FK_pabellon_celda", columns={"pabellon_id"})})
 * @ORM\Entity
 */
class Celda
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="capacidad", type="integer", nullable=false)
     */
    private $capacidad;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=50, nullable=false)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_base", type="string", length=1, nullable=false)
     */
    private $estadoBase;

    /**
     * @var Pabellon
     *
     * @ORM\ManyToOne(targetEntity="Pabellon")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pabellon_id", referencedColumnName="id")
     * })
     */
    private $pabellon;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCapacidad(): ?int
    {
        return $this->capacidad;
    }

    public function setCapacidad(int $capacidad): self
    {
        $this->capacidad = $capacidad;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getEstadoBase(): ?string
    {
        return $this->estadoBase;
    }

    public function setEstadoBase(string $estadoBase): self
    {
        $this->estadoBase = $estadoBase;

        return $this;
    }

    public function getPabellon(): ?Pabellon
    {
        return $this->pabellon;
    }

    public function setPabellon(?Pabellon $pabellon): self
    {
        $this->pabellon = $pabellon;

        return $this;
    }


}
