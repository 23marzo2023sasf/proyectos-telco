<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pabellonrelacion
 *
 * @ORM\Table(name="pabellonrelacion")
 * @ORM\Entity
 */
class Pabellonrelacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="pabellon_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $pabellonId;

    /**
     * @var int
     *
     * @ORM\Column(name="guardia_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $guardiaId;

    public function getPabellonId(): ?int
    {
        return $this->pabellonId;
    }

    public function getGuardiaId(): ?int
    {
        return $this->guardiaId;
    }


}
