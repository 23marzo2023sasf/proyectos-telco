<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GuardiaController extends AbstractController
{
    #[Route('/guardia', name: 'app_guardia')]
    public function index(): Response
    {
        return $this->render('guardia/index.html.twig', [
            'controller_name' => 'GuardiaController',
        ]);
    }
}
