USE db_sistema_presos;
DROP TABLE IF EXISTS pabellonRelacion;
DROP TABLE IF EXISTS celdaRelacion;
DROP TABLE IF EXISTS ficha;
DROP TABLE IF EXISTS celda;
DROP TABLE IF EXISTS pabellon;
DROP TABLE IF EXISTS usuario;


CREATE TABLE usuario( 
    id BIGINT AUTO_INCREMENT NOT NULL, 
    email VARCHAR(100) UNIQUE NOT NULL, 
    password VARCHAR(1) NOT NULL,
    roles JSON NOT NULL, 
    nombres VARCHAR(50) NOT NULL, 
    apellidos VARCHAR(50) NOT NULL, 
    tipo VARCHAR(25) NOT NULL, estado VARCHAR(1),
    estado_base VARCHAR(1),
    CONSTRAINT PK_usuario PRIMARY KEY (id)
    );

CREATE TABLE pabellon(
    id BIGINT AUTO_INCREMENT NOT NULL,
    descripcion VARCHAR(100) NOT NULL,
    capacidad INT NOT NULL,
    estado VARCHAR(25) NOT NULL,
    estado_base VARCHAR(1) NOT NULL,
    vigilante_id BIGINT NOT NULL,
    CONSTRAINT PK_pabellon PRIMARY KEY (id),
    CONSTRAINT FK_vigilante_pabellon FOREIGN KEY (vigilante_id) REFERENCES usuario (id)
   );

CREATE TABLE celda(
    id BIGINT AUTO_INCREMENT NOT NULL,
    capacidad INT NOT NULL,
    estado VARCHAR(50) NOT NULL,
    estado_base VARCHAR(1) NOT NULL,
    pabellon_id BIGINT NOT NULL,
    CONSTRAINT PK_celda PRIMARY KEY (id),
    CONSTRAINT FK_pabellon_celda FOREIGN KEY (pabellon_id) REFERENCES pabellon (id)
 );
CREATE TABLE ficha(
    id BIGINT AUTO_INCREMENT NOT NULL, 
    descripcion VARCHAR(100) NOT NULL,
    delitos VARCHAR(100) NOT NULL,
    sentencia VARCHAR(100) NOT NULL,
    nombres VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    estado_ficha VARCHAR(25) NOT NULL,
    estado_preso VARCHAR(25) NOT NULL,
    vigilante_id BIGINT NOT NULL,
    estado_base VARCHAR(1) NOT NULL,
    celda_id BIGINT,
    fecha_entrada DATE,
    fecha_salida DATE,
    CONSTRAINT PK_ficha PRIMARY KEY (id),
    CONSTRAINT FK_vigilante_ficha FOREIGN KEY (vigilante_id) REFERENCES usuario (id),
    CONSTRAINT FK_celda_ficha FOREIGN KEY (celda_id) REFERENCES celda (id)
);

  CREATE TABLE pabellonRelacion(
    pabellon_id INT NOT NULL,
    guardia_id INT NOT NULL,
    CONSTRAINT PK_relacion PRIMARY KEY (pabellon_id,guardia_id)
  );

  CREATE TABLE celdaRelacion(
    celda_id INT NOT NULL,
    guardia_id INT NOT NULL,
    CONSTRAINT PK_relacion PRIMARY KEY (celda_id,guardia_id)
  );  