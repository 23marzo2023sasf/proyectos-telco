<?php

namespace App\Entity;

use App\Repository\JuegosRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use App\Repository\TestingRepository;

#[ORM\Entity(repositoryClass: TestingRepository::class)]
class Testing
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $juegoId = null;

    #[ORM\Column]
    private ?int $userId = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $fe_creacion = null;


    public function __construct($idUsuario,$idJuego,$fecha){
        $this -> setJuegoId($idJuego);
        $this -> setUserId($idUsuario);
        $this -> setFeCreacion($fecha);
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJuegoId(): ?int
    {
        return $this->juegoId;
    }

    public function setJuegoId(int $juegoId): self
    {
        $this->juegoId = $juegoId;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getFeCreacion(): ?\DateTimeInterface
    {
        return $this->fe_creacion;
    }

    public function setFeCreacion(\DateTimeInterface $fe_creacion): self
    {
        $this->fe_creacion = $fe_creacion;

        return $this;
    }
    public function getCreador(UserRepository $userRepo, JuegosRepository $juegosRepository ): User
    {
        $juego= $juegosRepository->findOneBy(["id"=>($this->juegoId)]);
        $userEncontrado = $userRepo->findOneBy(["id"=>($juego->getUserId())]);
         return $userEncontrado;
    }

    
}
