<?php

namespace App\Controller;

use App\Entity\Juegos;
use App\Entity\Testing;
use App\Form\RegisterJuegoType;
use App\Repository\UserRepository;
use App\Repository\JuegosRepository;
use App\Repository\TestingRepository;
use DateTimeZone;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TesterDashboardController extends AbstractController
{
    #[Route('/tester/dashboard', name: 'app_tester_dashboard')]
    public function index(JuegosRepository $juegoRep): Response
    {
        
        $arrayJuegos =array();
        $arrayJuegos = array_merge($juegoRep ->findBy(["estado" => "Activo"])
                                 );
        return $this->render('tester_dashboard/index.html.twig', [
            'controller_name' => 'TesterDashboardController',
            'listJuegos' => $arrayJuegos
        ]);
    }
    #[Route('/tester/dashboard/Test/{id}', name: 'app_tester_dashboard_test')]
    public function Test(Request $request,Juegos $juego, UserRepository $userRepo, JuegosRepository $juegoRep,TestingRepository $testRep): Response
    {
        $mensaje="jj";
        $estado="jj";
        $email = $request->getSession()->get('_security.last_username', '');
        $objUsuario = $userRepo -> findOneBy(["email" => $email]);
        $form = $this ->createForm(RegisterJuegoType::class, $juego);
        $form -> handleRequest($request);
        $juego = $form->getData();
        $listaTests = $testRep -> findAll();
        foreach ($listaTests as $test)
        {
         
            if (($juego ->getId() == $test->getJuegoId()))
            {
                    $mensaje = "Error: Usted ya se encuentra probando este juego";
                    $estado = "error";
                
            }
            elseif(($juego->getCreador($userRepo) == $test->getCreador($userRepo,$juegoRep)  ))
            {
                $mensaje = "Error: Usted ya se encuentra probando un juego con este mismo desarrollador";
                    $estado = "error";
            }
            
        }
            
            if($estado!= "error")
            {
                $mensaje="Se te ha permitido testear el juego, comenzando descarga del juego";
                $estado="success";
                date_default_timezone_set("America/Lima");
                $fecha=date("d-m-Y");
                $testNuevo = new Testing($objUsuario->getId(),
                                         $juego->getId(),
                                         date_create($fecha));
                $testRep ->save($testNuevo,true);
            }
            $this -> addFlash($estado, $mensaje);
            return $this->redirectToRoute('app_tester_dashboard');
            
        
        
    }
    
}
