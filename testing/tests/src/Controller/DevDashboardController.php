<?php

namespace App\Controller;

use App\Entity\Juegos;
use App\Form\RegisterJuegoType;
use App\Repository\UserRepository;

use App\Repository\JuegosRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Security\LoginCustomAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DevDashboardController extends AbstractController
{
    #[Route('/dev/dashboard', name: 'app_dev_dashboard')]
    public function index(Request $request, JuegosRepository $juegoRep): Response
    {
        //$user = $request ->getUser();
        
        return $this->render('dev_dashboard/index.html.twig', [
            'listJuegos'=>$juegoRep->findAll(),
        ]);
    }

    #[Route('/dev/dashboard/register', name: 'app_dev_dashboard_register')]
    public function register(Request $request, JuegosRepository $juegoRep,ManagerRegistry $doctrine, UserRepository $userRepository): Response
    {
        //$user = $request->request ->ge()
        $email = $request->getSession()->get('_security.last_username', '');
        //var_dump($email);
        $user = $userRepository->findOneBy(["email"=>$email]);
        $juego=new Juegos();
        $form =$this->createForm(RegisterJuegoType::class, $juego);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $juego=$form->getData();
            $juego->setUserId($user->getId());
            $juego->setEstado("EnDesarrollo");
            $em=$doctrine->getManager();
            $em->persist($juego);
            $em->flush();
            $this->addFlash("success","Registro de Juego Exitoso");
            return $this->redirectToRoute('app_dev_dashboard');
        }
        return $this->render('dev_dashboard/registerJuego.html.twig', [
            'formulario'=>$form->createView(),
        ]);
    }

    #[Route('/dev/dashboard/edit/{id}', name: 'app_dev_dashboard_edit')]
    public function edit(Request $request,Juegos $juego, JuegosRepository $juegoRep,UserRepository $userRepo): Response
    {
        
        $mensaje="jj";
        $estado="jj";
        $email = $request->getSession()->get('_security.last_username', '');
        
        $objUsuario = $userRepo->findOneBy(["email"=>$email]);
        $form = $this->createForm(RegisterJuegoType::class, $juego, array('accion'=>'editJuego'));
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $juego = $form->getData();
            if ($juego->getEstado() == "Activo")
            {
                $totalJuegos = $juegoRep->totalByUserIdAndEstado($objUsuario->getId(),"Activo");
                if($totalJuegos != null && $totalJuegos >=2)
                {
                    $mensaje="Error: No puede tener mas de dos juegos activos para test";
                    $estado="error";
                }
            }
            elseif ($juego->getEstado() == "EnDesarrollo" )
            {
                    $mensaje="Error: No puede cambiar el estado a En Desarrollo";
                    $estado="error";
            }
            if($estado!= "error")
            {
                $mensaje="Juego actualizado correctamente";
                $estado="success";
                $juegoRep->save($juego,true);
            }
            $this->addFlash($estado, $mensaje);
            return $this->redirectToRoute('app_dev_dashboard');
            
        }
        return $this->render('dev_dashboard/registerJuego.html.twig', [
            'formulario'=>$form->createView(),
        ]);
    }

    #[Route('/dev/dashboard/delete/{id}', name: 'app_dev_dashboard_delete')]
    public function delete(Juegos $juego, JuegosRepository $juegoRep): Response
    {
        if($juego->getEstado() == "Activo")    
        {
            $this->addFlash("error", "Error: No puede eliminar un juego que este siendo testeado");
        }
        else 
        {
           $juegoRep->remove($juego,true);
           $this->addFlash("success", "Juego eliminado con exito"); 
        }
        
        return $this->redirectToRoute('app_dev_dashboard');
    }

}
