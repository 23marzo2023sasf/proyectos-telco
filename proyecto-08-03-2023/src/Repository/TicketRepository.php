<?php

namespace App\Repository;

use App\Entity\Ticket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Ticket>
 *
 * @method Ticket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ticket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ticket[]    findAll()
 * @method Ticket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    public function save(Ticket $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Ticket $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function getTicketUsuario($idUser): ?array
    {
        //se hace un left join para obtener las tickets aun cuando no tengan registrado un tecnico Id
        $strSql = "SELECT tickets.id,
                   tickets.fe_creacion,
                   tickets.motivo,
                   tickets.estado,
                   tipoTrabajo.descripcion descripciontipoTrabajo,
                   userTecnico.nombres nombre_tecnico,
                   userTecnico.apellidos apellido_tecnico
                   FROM App\Entity\Ticket tickets
                   LEFT JOIN App\Entity\User userTecnico
                   WITH tickets.tecnico_id = userTecnico.id
                   LEFT JOIN App\Entity\TipoTrabajo tipoTrabajo
                   WITH tickets.tipo_trabajo_id = tipoTrabajo.id
                   WHERE tickets.cliente_id = :cliente ";
        return $this->_em->createQuery($strSql)
                    ->setParameter('cliente',$idUser)
                    ->getResult();         
    }
    public function getTicketTecnico($idUser): ?array
    {
        //se hace un left join para obtener las tickets aun cuando no tengan registrado un tecnico Id
        $strSql = "SELECT tickets.id,
                   tickets.fe_creacion,
                   tickets.motivo,
                   tickets.estado,
                   tickets.duracion,
                   tipoTrabajo.descripcion descripciontipoTrabajo,
                   tipoTrabajo.precio valorTicket,
                   userTecnico.nombres nombre_tecnico,
                   userTecnico.apellidos apellido_tecnico,
                   userCliente.nombres cliente_nombres,
                   userCliente.apellidos cliente_apellidos
                   FROM App\Entity\Ticket tickets
                   JOIN App\Entity\User userCliente
                   WITH tickets.cliente_id = userCliente.id
                   LEFT JOIN App\Entity\User userTecnico
                   WITH tickets.tecnico_id = userTecnico.id
                   LEFT JOIN App\Entity\TipoTrabajo tipoTrabajo
                   WITH tickets.tipo_trabajo_id = tipoTrabajo.id
                   WHERE tickets.tecnico_id =:tecnico ";
        return $this->_em->createQuery($strSql)
                    ->setParameter('tecnico',$idUser)
                    ->getResult();         
    }
    public function getTicketEstado($estado): ?array
    {
        //se hace un left join para obtener las tickets aun cuando no tengan registrado un tecnico Id
        $strSql = "SELECT tickets.id,
                   tickets.fe_creacion,
                   tickets.motivo,
                   tickets.estado,
                   tickets.duracion,
                   tipoTrabajo.descripcion descripciontipoTrabajo,
                   tipoTrabajo.precio valorTrabajo,
                   userTecnico.nombres nombre_tecnico,
                   userTecnico.apellidos apellido_tecnico,
                   userCliente.nombres cliente_nombres,
                   userCliente.apellidos cliente_apellidos
                   FROM App\Entity\Ticket tickets
                   JOIN App\Entity\User userCliente
                   WITH tickets.cliente_id = userCliente.id
                   LEFT JOIN App\Entity\User userTecnico
                   WITH tickets.tecnico_id = userTecnico.id
                   LEFT JOIN App\Entity\TipoTrabajo tipoTrabajo
                   WITH tickets.tipo_trabajo_id = tipoTrabajo.id
                   WHERE tickets.estado =:estado ";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado',$estado)
                    ->getResult();         
    }

//    /**
//     * @return Ticket[] Returns an array of Ticket objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Ticket
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
