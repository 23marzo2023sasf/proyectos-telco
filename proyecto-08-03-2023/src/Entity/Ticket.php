<?php

namespace App\Entity;

use App\Repository\TicketRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TicketRepository::class)]
class Ticket
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $fe_creacion = null;

    #[ORM\Column(length: 100)]
    private ?string $motivo = null;

    #[ORM\Column(length: 50)]
    private ?string $estado = null;

    #[ORM\Column(nullable: true)]
    private ?int $tipo_trabajo_id = null;

    #[ORM\Column]
    private ?int $cliente_id = null;

    #[ORM\Column(nullable: true)]
    private ?int $tecnico_id = null;

    #[ORM\Column(nullable: true)]
    private ?int $duracion = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $estado_base = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFeCreacion(): ?\DateTimeInterface
    {
        return $this->fe_creacion;
    }

    public function setFeCreacion(?\DateTimeInterface $fe_creacion): self
    {
        $this->fe_creacion = $fe_creacion;

        return $this;
    }

    public function getMotivo(): ?string
    {
        return $this->motivo;
    }

    public function setMotivo(string $motivo): self
    {
        $this->motivo = $motivo;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getTipoTrabajoId(): ?int
    {
        return $this->tipo_trabajo_id;
    }

    public function setTipoTrabajoId(?int $tipo_trabajo_id): self
    {
        $this->tipo_trabajo_id = $tipo_trabajo_id;

        return $this;
    }

    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    public function setClienteId(int $cliente_id): self
    {
        $this->cliente_id = $cliente_id;

        return $this;
    }

    public function getTecnicoId(): ?int
    {
        return $this->tecnico_id;
    }

    public function setTecnicoId(?int $tecnico_id): self
    {
        $this->tecnico_id = $tecnico_id;

        return $this;
    }

    public function getDuracion(): ?int
    {
        return $this->duracion;
    }

    public function setDuracion(?int $duracion): self
    {
        $this->duracion = $duracion;

        return $this;
    }

    public function getEstadoBase(): ?string
    {
        return $this->estado_base;
    }

    public function setEstadoBase(?string $estado_base): self
    {
        $this->estado_base = $estado_base;

        return $this;
    }
}
