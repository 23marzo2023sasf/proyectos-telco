<?php

namespace App\Controller;

use App\Entity\Factura;
use App\Form\RegisterFacturaType;
use App\Repository\UserRepository;
use App\Repository\FacturaRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CajeroDashboardController extends AbstractController
{
    #[Route('/cajero/dashboard', name: 'app_cajero_dashboard')]
    public function index(Request $request,FacturaRepository $facturaRepository): Response
    {
        return $this->render('cajero_dashboard/index.html.twig', [
            'listFacturas'=>$facturaRepository->getFacturaCajero(),
        ]);
    }
    #[Route('/cajero/dashboard/cobrar/{id}', name: 'app_cajero_dashboard_cobrar')]
    public function cobrar(Request $request,Factura $factura,FacturaRepository $facturaRepository,UserRepository $userRepository,ManagerRegistry $doctrine): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        $objUsuario = $userRepository->findOneBy(["email"=>$email]);
        $form = $this->createForm(RegisterFacturaType::class, $factura);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $factura=$form->getData();
            $factura->setCajeroId($objUsuario->getId());
            $factura->setEstado("Pagada");
           // $ticket->setEstado("En Espera");
            $this->addFlash("success", "Exitos: La factura fue pagada");
            $facturaRepository->save($factura,true);
            return $this->redirectToRoute('app_cajero_dashboard');
        }
        return $this->render('cajero_dashboard/pagar.html.twig', [
            'formulario'=>$form->createView()]);
    }
}
