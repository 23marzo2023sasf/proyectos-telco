<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Entity\Factura;
use App\Form\RegisterTicketType;
use App\Repository\TipoTrabajoRepository;
use App\Repository\UserRepository;
use App\Repository\TicketRepository;
use App\Repository\FacturaRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TecnicoDashboardController extends AbstractController
{
    #[Route('/tecnico/dashboard', name: 'app_tecnico_dashboard')]
    public function index(Request $request, TicketRepository $ticketRepository, UserRepository $userRepository): Response
    {
        
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $userRepository->findOneBy(["email"=>$email]);
        return $this->render('tecnico_dashboard/index.html.twig', [
            'listTickets'=>$ticketRepository->getTicketTecnico($user->getId()),
        ]);
    }
    #[Route('/tecnico/dashboard/tickets', name: 'app_tecnico_dashboard_tickets')]
    public function citas(Request $request, TicketRepository $ticketRepository, UserRepository $userRepository): Response
    {
        
        return $this->render('tecnico_dashboard/ticketsSinAtender.html.twig', [
            'listTickets'=>$ticketRepository->getTicketEstado("En Espera"),
        ]);
    }
    #[Route('/tecnico/dashboard/atender/{id}', name: 'app_tecnico_dashboard_atender')]
    public function atender(Request $request,TipoTrabajoRepository $tipoTrabajoRepository,Ticket $ticket, FacturaRepository $facturaRepository,TicketRepository $ticketRepository,ManagerRegistry $doctrine, UserRepository $userRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        $arrayTiposTrabajos = $tipoTrabajoRepository->findAll();
        $objUsuario = $userRepository->findOneBy(["email"=>$email]);
        $form = $this->createForm(RegisterTicketType::class, $ticket, ['accion'=>'editTecnico','arrayTiposTrabajos'=>$arrayTiposTrabajos]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $ticket=$form->getData();
            $ticket->setTecnicoId($objUsuario->getId());
            $ticket->setEstado("Atendido");
           // $ticket->setEstado("En Espera");
            $this->addFlash("success", "Exitos: El ticket fue atendido, ahora se eviara a centro de facturacion");
            $ticketRepository->save($ticket,true);
            
            $nuevaFactura=new Factura();
            $tipo=$tipoTrabajoRepository->findOneBy(["id"=>$ticket->getTipoTrabajoId()]);
            $nuevaFactura->setTotal(($ticket->getDuracion())*40.00);
            //$nuevaFactura->setFeCreacion(new \Datetime());
            $nuevaFactura->setTicketId($ticket->getId());
            $nuevaFactura->setEstado("No Pagada");
            //$facturaRep=$doctrine->getRepository("App\Entity\Factura");
            $facturaRepository->save($nuevaFactura,true);
            
        
          return $this->redirectToRoute('app_tecnico_dashboard');  
        }
        return $this->render('tecnico_dashboard/editarTicket.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }
}
