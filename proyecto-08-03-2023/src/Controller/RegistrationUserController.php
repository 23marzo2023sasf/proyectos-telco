<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterUserType;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class RegistrationUserController extends AbstractController
{
    #[Route('/registration/user', name: 'app_registration_user')]
    public function index(Request $request, UserPasswordHasherInterface $encoderPassword, ManagerRegistry $doctrine, UserRepository $userRepository): Response
    {
        $user = new User(User :: ROLE_USER,"Nuevo");
        $form = $this -> createForm(RegisterUserType :: class, $user);
        $form -> handleRequest($request);
        if($form ->isSubmitted() && $form ->isValid())
        {
            $user=$form ->getData();
            $user -> setPassword($encoderPassword ->hashPassword($user,$form["password"]->getData()));
            if ($user->getTipoUsuario() == "Tecnico")
            {
                $user -> setRoles([USER ::ROLE_TECNICO]);
            }
            elseif ($user->getTipoUsuario() == "Cliente" )
            {
                $user -> setRoles([USER ::ROLE_CLIENTE]);
            }
            elseif ($user->getTipoUsuario() == "Admin" )
            {
                $user -> setRoles([USER ::ROLE_ADMIN]);
            }
            elseif ($user->getTipoUsuario() == "Cajero" )
            {
                $user -> setRoles([USER ::ROLE_CAJERO]);
            }
            $userRepository->save($user,true);    
            return $this-> redirectToRoute('app_login');
                
            
        }
        return $this->render('registration_user/index.html.twig', [
            'formulario' => $form ->createView(),
        ]);
    }
}
