<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Form\RegisterTicketType;
use App\Repository\UserRepository;
use App\Repository\TicketRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ClienteDashboardController extends AbstractController
{
    #[Route('/cliente/dashboard', name: 'app_cliente_dashboard')]
    public function index(Request $request, TicketRepository $ticketRepository, UserRepository $userRepository): Response
    {
        
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $userRepository->findOneBy(["email"=>$email]);
        return $this->render('cliente_dashboard/index.html.twig', [
            'listTickets'=>$ticketRepository->getTicketUsuario($user->getId()),
        ]);
    }

    #[Route('/cliente/dashboard/register', name: 'app_cliente_dashboard_register')]
    public function agregar(Request $request, TicketRepository $ticketRepository,ManagerRegistry $doctrine, UserRepository $userRepository): Response
    {
        //$user = $request->request ->ge()
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $userRepository->findOneBy(["email"=>$email]);
        $ticket=new Ticket();
        $form =$this->createForm(RegisterTicketType::class, $ticket);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $ticket=$form->getData();
            $ticket->setClienteId($user->getId());
            $ticket->setEstado("En Espera");
            $ticket->setEstadoBase("Activo");
            //$ticket->setDuracion(30);
            $ticket ->setFeCreacion(new \Datetime());
            $em=$doctrine->getManager();
            $em->persist($ticket);
            $em->flush();
            $this->addFlash("success","Registro de Ticket Exitoso");
            return $this->redirectToRoute('app_cliente_dashboard');
        }
        return $this->render('cliente_dashboard/registerTicket.html.twig', [
            'formulario'=>$form->createView(),
        ]);
    }
    #[Route('/cliente/dashboard/edit/{id}', name: 'app_cliente_dashboard_edit')]
    public function edit(Request $request,Ticket $ticket, TicketRepository $ticketRepository,ManagerRegistry $doctrine, UserRepository $userRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        
        $objUsuario = $userRepository->findOneBy(["email"=>$email]);
        $form = $this->createForm(RegisterTicketType::class, $ticket);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if($ticket->getEstado() == "En Espera")    
        {
            $ticket=$form->getData();
            $ticket->setClienteId($objUsuario->getId());
           // $ticket->setEstado("En Espera");
            $this->addFlash("success", "Exitos: El ticket fue editado con exito");
            $ticketRepository->save($ticket,true);
            
        }
        else 
        {
            $this->addFlash("Error", "Error: No puede eliminar un ticket que ya ha sido atendido"); 
        }
          return $this->redirectToRoute('app_cliente_dashboard');  
        }
        return $this->render('cliente_dashboard/registerTicket.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }
    #[Route('/cliente/dashboard/delete/{id}', name: 'app_cliente_dashboard_delete')]
    public function delete(Ticket $ticket, TicketRepository $ticketRep): Response
    {
        if($ticket->getEstado() == "En Espera")    
        {
            $ticket->setEstadoBase("Inactivo");
            $this->addFlash("success", "ticket eliminado con exito");
            
        }
        else 
        {
            $this->addFlash("error", "Error: No puede eliminar un ticket que este siendo testeado"); 
        }
        
        return $this->redirectToRoute('app_cliente_dashboard');
    }
}
