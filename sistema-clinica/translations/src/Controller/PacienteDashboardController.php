<?php

namespace App\Controller;

use App\Entity\Cita;
use App\Form\RegisterCitaType;
use App\Repository\CitaRepository;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PacienteDashboardController extends AbstractController
{
    #[Route('/paciente/dashboard', name: 'app_paciente_dashboard')]
    public function index(Request $request, CitaRepository $citaRep, UserRepository $userRepository): Response
    {
        //$user = $request ->getUser();
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $userRepository->findOneBy(["email"=>$email]);
        return $this->render('paciente_dashboard/index.html.twig', [
            'listCitas'=>$citaRep->getCitaUsuario($user->getId()),
        ]);
    }
    #[Route('/paciente/dashboard/register', name: 'app_paciente_dashboard_register')]
    public function agregar(Request $request, CitaRepository $citaRep,ManagerRegistry $doctrine, UserRepository $userRepository): Response
    {
        //$user = $request->request ->ge()
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $userRepository->findOneBy(["email"=>$email]);
        $cita=new Cita();
        $form =$this->createForm(RegisterCitaType::class, $cita);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $cita=$form->getData();
            $cita->setPacienteId($user->getId());
            $cita->setEstado("En Espera");
            //$cita->setDuracion(30);
            $cita ->setFeCreacion(new \Datetime());
            $em=$doctrine->getManager();
            $em->persist($cita);
            $em->flush();
            $this->addFlash("success","Registro de Cita Exitosa");
            return $this->redirectToRoute('app_paciente_dashboard');
        }
        return $this->render('paciente_dashboard/registerCita.html.twig', [
            'formulario'=>$form->createView(),
        ]);
    }

    #[Route('/paciente/dashboard/edit/{id}', name: 'app_paciente_dashboard_edit')]
    public function edit(Request $request,Cita $cita, CitaRepository $citaRep,ManagerRegistry $doctrine, UserRepository $userRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        
        $objUsuario = $userRepository->findOneBy(["email"=>$email]);
        $form = $this->createForm(RegisterCitaType::class, $cita);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if($cita->getEstado() == "En Espera")    
        {
            $cita=$form->getData();
            $cita->setPacienteId($objUsuario->getId());
           // $cita->setEstado("En Espera");
            $this->addFlash("success", "Exitos: La cita fue creada con exito, cuando su cita sea agendada se le enviara un mensaje");
            $citaRep->save($cita,true);
            
        }
        else 
        {
            $this->addFlash("Error", "Error: No puede eliminar una cita que ya se encuentra agendada"); 
        }
          return $this->redirectToRoute('app_paciente_dashboard');  
        }
        return $this->render('paciente_dashboard/registerCita.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }

    #[Route('/paciente/dashboard/delete/{id}', name: 'app_paciente_dashboard_delete')]
    public function delete(Cita $cita, CitaRepository $citaRep): Response
    {
        if($cita->getEstado() == "En Espera")    
        {
            $citaRep->remove($cita,true);
            $this->addFlash("success", "Juego eliminado con exito");
            
        }
        else 
        {
            $this->addFlash("error", "Error: No puede eliminar un juego que este siendo testeado"); 
        }
        
        return $this->redirectToRoute('app_paciente_dashboard');
    }
}
