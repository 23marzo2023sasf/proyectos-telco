<?php

namespace App\Controller;

use App\Entity\Factura;
use App\Repository\FacturaRepository;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CajeroController extends AbstractController
{
    #[Route('/cajero', name: 'app_cajero')]
    public function index(Request $request,FacturaRepository $facturaRepository): Response
    {
        
        return $this->render('cajero/index.html.twig', [
            'listFacturas'=>$facturaRepository->getFacturaCajero(),
        ]);
    }
    #[Route('/cajero/cobrar/{id}', name: 'app_cajero_cobrar')]
    public function cobrar(Request $request,Factura $factura,FacturaRepository $facturaRepository,UserRepository $userRepository,ManagerRegistry $doctrine): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        $objUsuario = $userRepository->findOneBy(["email"=>$email]);
        $factura->setCajeroId($objUsuario->getId());
        $factura->setEstado("Pagada");
        return $this->redirectToRoute('app_med_dashboard');
    }
}
