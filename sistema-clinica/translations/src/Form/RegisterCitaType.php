<?php

namespace App\Form;

use App\Entity\Cita;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterCitaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        
        $builder
            //->add('fe_creacion')
            ->add('motivo',TextType::class)
            //->add('estado',TextType::class)
            //->add('tipo_cita_id')
           // ->add('paciente_id')
            //->add('medico_id')
          
            
            ;
        if ($options['accion']=='editMed')
        {
            $builder
                   ->add('fe_creacion', DateTimeType::class,["widget"=>"single_text","disabled"=>true])
                   ->add('motivo',TextType::class,["disabled"=>true])
                   ->add('estado',TextType::class,["disabled"=>true])
                   ->add('tipo_Cita',ChoiceType::class,
                                    ['choices'=>$options["arrayTiposCitas"],//array que se envia desde el controlador
                                     'choice_value'=>'id',//atributo de valor que se setea al seleccionar una opcion
                                     'choice_label'=>'descripcion',//lo que se mostrara en pantala en las opciones
                                     'label'=>'Tipo de Cita',//lo que se muestra en pantalla por defecto
                                     'mapped'=>false])//debido a que no hay relaccion entre las entidades, enviamos bandera para que no se mapee
                   ->add('duracion',NumberType::class);
        } 
      $builder->add('save', SubmitType :: class, ['label' => 'Registrar']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Cita::class,
            'accion' => 'crearJuego',
            'arrayTiposCitas'=>array()

        ]);
    }
}
