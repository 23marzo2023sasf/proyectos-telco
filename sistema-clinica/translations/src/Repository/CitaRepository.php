<?php

namespace App\Repository;

use App\Entity\Cita;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Cita>
 *
 * @method Cita|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cita|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cita[]    findAll()
 * @method Cita[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CitaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cita::class);
    }

    public function save(Cita $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Cita $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function getCitaUsuario($idUser): ?array
    {
        //se hace un left join para obtener las citas aun cuando no tengan registrado un medico Id
        $strSql = "SELECT citas.id,
                   citas.fe_creacion,
                   citas.motivo,
                   citas.estado,
                   tipoCita.descripcion descripcionTipoCita,
                   userMedico.nombres nombre_medico,
                   userMedico.apellidos apellido_medico
                   FROM App\Entity\Cita citas
                   LEFT JOIN App\Entity\User userMedico
                   WITH citas.medico_id = userMedico.id
                   LEFT JOIN App\Entity\TipoCita tipoCita
                   WITH citas.tipo_cita_id = tipoCita.id
                   WHERE citas.paciente_id = :paciente";
        return $this->_em->createQuery($strSql)
                    ->setParameter('paciente',$idUser)
                    ->getResult();         
    }
    public function getCitaMedico($idUser): ?array
    {
        //se hace un left join para obtener las citas aun cuando no tengan registrado un medico Id
        $strSql = "SELECT citas.id,
                   citas.fe_creacion,
                   citas.motivo,
                   citas.estado,
                   citas.duracion,
                   tipoCita.descripcion descripcionTipoCita,
                   tipoCita.precio valorCita,
                   userMedico.nombres nombre_medico,
                   userMedico.apellidos apellido_medico,
                   userPaciente.nombres paci_nombres,
                   userPaciente.apellidos paci_apellidos
                   FROM App\Entity\Cita citas
                   JOIN App\Entity\User userPaciente
                   WITH citas.paciente_id = userPaciente.id
                   LEFT JOIN App\Entity\User userMedico
                   WITH citas.medico_id = userMedico.id
                   LEFT JOIN App\Entity\TipoCita tipoCita
                   WITH citas.tipo_cita_id = tipoCita.id
                   WHERE citas.medico_id =:medico";
        return $this->_em->createQuery($strSql)
                    ->setParameter('medico',$idUser)
                    ->getResult();         
    }
    public function getCitaEstado($estado): ?array
    {
        //se hace un left join para obtener las citas aun cuando no tengan registrado un medico Id
        $strSql = "SELECT citas.id,
                   citas.fe_creacion,
                   citas.motivo,
                   citas.estado,
                   citas.duracion,
                   tipoCita.descripcion descripcionTipoCita,
                   tipoCita.precio valorCita,
                   userMedico.nombres nombre_medico,
                   userMedico.apellidos apellido_medico,
                   userPaciente.nombres paci_nombres,
                   userPaciente.apellidos paci_apellidos
                   FROM App\Entity\Cita citas
                   JOIN App\Entity\User userPaciente
                   WITH citas.paciente_id = userPaciente.id
                   LEFT JOIN App\Entity\User userMedico
                   WITH citas.medico_id = userMedico.id
                   LEFT JOIN App\Entity\TipoCita tipoCita
                   WITH citas.tipo_cita_id = tipoCita.id
                   WHERE citas.estado =:estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado',$estado)
                    ->getResult();         
    }

//    /**
//     * @return Cita[] Returns an array of Cita objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Cita
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
