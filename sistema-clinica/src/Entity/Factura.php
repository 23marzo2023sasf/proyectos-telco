<?php

namespace App\Entity;

use App\Repository\FacturaRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FacturaRepository::class)]
class Factura
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $fe_creacion = null;

    #[ORM\Column]
    private ?float $total = null;

    #[ORM\Column]
    private ?int $cita_id = null;

    #[ORM\Column(nullable: true)]
    private ?int $cajero_id = null;

    #[ORM\Column(length: 50)]
    private ?string $estado = null;

   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFeCreacion(): ?\DateTimeInterface
    {
        return $this->fe_creacion;
    }

    public function setFeCreacion(\DateTimeInterface $fe_creacion): self
    {
        $this->fe_creacion = $fe_creacion;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getCitaId(): ?int
    {
        return $this->cita_id;
    }

    public function setCitaId(int $cita_id): self
    {
        $this->cita_id = $cita_id;

        return $this;
    }

    public function getCajeroId(): ?int
    {
        return $this->cajero_id;
    }

    public function setCajeroId(int $cajero_id): self
    {
        $this->cajero_id = $cajero_id;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

  
}
