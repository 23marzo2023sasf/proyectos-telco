<?php

namespace App\Entity;

use App\Repository\CitaRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CitaRepository::class)]
class Cita
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE,nullable: true)]
    private ?\DateTimeInterface $fe_creacion = null;

    #[ORM\Column(length: 100)]
    private ?string $motivo = null;

    #[ORM\Column(length: 100)]
    private ?string $estado = null;

    #[ORM\Column(nullable: true)]
    private ?int $tipo_cita_id = null;

    #[ORM\Column]
    private ?int $paciente_id = null;

    #[ORM\Column(nullable: true)]
    private ?int $medico_id = null;

    #[ORM\Column(nullable: true)]
    private ?int $duracion = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFeCreacion(): ?\DateTimeInterface
    {
        return $this->fe_creacion;
    }

    public function setFeCreacion(\DateTimeInterface $fe_creacion): self
    {
        $this->fe_creacion = $fe_creacion;

        return $this;
    }

    public function getMotivo(): ?string
    {
        return $this->motivo;
    }

    public function setMotivo(string $motivo): self
    {
        $this->motivo = $motivo;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getTipoCitaId(): ?int
    {
        return $this->tipo_cita_id;
    }

    public function setTipoCitaId(int $tipo_cita_id): self
    {
        $this->tipo_cita_id = $tipo_cita_id;

        return $this;
    }

    public function getPacienteId(): ?int
    {
        return $this->paciente_id;
    }

    public function setPacienteId(int $paciente_id): self
    {
        $this->paciente_id = $paciente_id;

        return $this;
    }

    public function getMedicoId(): ?int
    {
        return $this->medico_id;
    }

    public function setMedicoId(?int $medico_id): self
    {
        $this->medico_id = $medico_id;

        return $this;
    }

    public function getDuracion(): ?int
    {
        return $this->duracion;
    }

    public function setDuracion(?int $duracion): self
    {
        $this->duracion = $duracion;

        return $this;
    }
}
