<?php

namespace App\Controller;

use App\Entity\Cita;
use App\Entity\Factura;
use App\Form\RegisterCitaType;
use App\Repository\CitaRepository;
use App\Repository\UserRepository;
use App\Repository\FacturaRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MedDashboardController extends AbstractController
{
    #[Route('/med/dashboard', name: 'app_med_dashboard')]
    public function index(Request $request, CitaRepository $citaRep, UserRepository $userRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $userRepository->findOneBy(["email"=>$email]);
        return $this->render('med_dashboard/index.html.twig', [
            'listCitas'=>$citaRep->getCitaMedico($user->getId()),
        ]);
    }
    #[Route('/med/dashboard/citas', name: 'app_med_dashboard_citas')]
    public function citas(Request $request, CitaRepository $citaRep, UserRepository $userRepository): Response
    {
        
        return $this->render('med_dashboard/citasSinAtender.html.twig', [
            'listCitas'=>$citaRep->getCitaEstado("En Espera"),
        ]);
    }
    #[Route('/med/dashboard/atender/{id}', name: 'app_med_dashboard_atender')]
    public function atender(Request $request,Cita $cita, FacturaRepository $facturaRepository,CitaRepository $citaRep,ManagerRegistry $doctrine, UserRepository $userRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        $arrayTiposCitas = $doctrine->getRepository("App\Entity\TipoCita")->findAll();
        $objUsuario = $userRepository->findOneBy(["email"=>$email]);
        $form = $this->createForm(RegisterCitaType::class, $cita, ['accion'=>'editMed','arrayTiposCitas'=>$arrayTiposCitas]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $cita=$form->getData();
            $cita->setMedicoId($objUsuario->getId());
            $cita->setEstado("Atendido");
           // $cita->setEstado("En Espera");
            $this->addFlash("success", "Exitos: La cita fue creada con exito, cuando su cita sea agendada se le enviara un mensaje");
            $citaRep->save($cita,true);
            
            $nuevaFactura=new Factura();
            $nuevaFactura->setFeCreacion(new \Datetime());
            $nuevaFactura->setCitaId($cita->getId());
            $nuevaFactura->setEstado("No Pagada");
            //$facturaRep=$doctrine->getRepository("App\Entity\Factura");
            $facturaRepository->save($nuevaFactura,true);
            
        
          return $this->redirectToRoute('app_med_dashboard');  
        }
        return $this->render('med_dashboard/editarCita.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }
}

