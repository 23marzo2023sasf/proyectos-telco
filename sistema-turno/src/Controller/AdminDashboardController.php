<?php

namespace App\Controller;

use App\Entity\Usuario;

use App\Repository\FacturaRepository;
use App\Repository\UsuarioRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\RegisterIncidenteType;

class AdminDashboardController extends AbstractController
{
    #[Route('/admin/dashboard', name: 'app_admin_dashboard')]
    public function index(Request $request ,UsuarioRepository $usuarioRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $usuario = $usuarioRepository->findOneBy(["email"=>$email]);
        return $this->render('admin_dashboard/index.html.twig', [
            'listUsuarios'=>$usuarioRepository->findAll(),
        ]);
    }
    #[Route('/admin/dashboard/facturas', name: 'app_admin_dashboard_facturas')]
    public function facturas(Request $request ,FacturaRepository $facturaRepository): Response
    {
        return $this->render('admin_dashboard/facturas.html.twig', [
            'listFacturas'=>$facturaRepository->getFacturaAdmin(),
        ]);
    }

    
}

