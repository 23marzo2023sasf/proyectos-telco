<?php

namespace App\Controller;

use App\Entity\Turno;
use App\Form\FiltroFacturaType;
use App\Repository\FacturaRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContadorDashboardController extends AbstractController
{
    #[Route('/contador/dashboard', name: 'app_contador_dashboard')]
    public function index(Request $request,FacturaRepository $facturaRepository): Response
    {
        return $this->render('contador_dashboard/index.html.twig', [
            'listFacturas'=>$facturaRepository->getFacturaAdmin(),
        ]);
    }
    #[Route('/contador/dashboard/filtrarEspecialidad', name: 'app_contador_dashboard?filtrarEspecialidad')]
    public function filtrarEspecialidad(Request $request ,FacturaRepository $facturaRepository): Response
    {
        $turno=new Turno();
        $form =$this->createForm(FiltroFacturaType::class, $turno);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $turno=$form->getData();
            $idTipo=$form["tipo_id"]->getData();
            $fecha=$form["fecha"]->getData();
            //$turno->setDuracion(30);
            //$turno ->setFeCreacion(new \Datetime());
            
            $this->addFlash("success","Registro de Turno Exitoso");
            return $this->render('contador_dashboard/index.html.twig', [
                'listFacturas'=>$facturaRepository->getFacturaEspecialidad($idTipo),
            ]);
        }
        return $this->render('contador_dashboard/index.html.twig', [
            'formulario'=>$form->createView(),
        ]);
    }
}
