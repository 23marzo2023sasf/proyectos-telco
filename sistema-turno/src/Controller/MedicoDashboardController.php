<?php

namespace App\Controller;

use App\Entity\Turno;
use App\Entity\Factura;
use App\Form\RegisterTurnoType;
use App\Repository\TipoTurnoRepository;
use App\Repository\TurnoRepository;
use App\Repository\FacturaRepository;
use App\Repository\UsuarioRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MedicoDashboardController extends AbstractController
{
    #[Route('/medico/dashboard', name: 'app_medico_dashboard')]
    public function index(Request $request, TurnoRepository $turnoRepository, UsuarioRepository $usuarioRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $usuarioRepository->findOneBy(["email"=>$email]);
        return $this->render('medico_dashboard/index.html.twig', [
            'listTurnos'=>$turnoRepository->getTurnoMedico($user->getId()),
        ]);
    }
    #[Route('/medico/dashboard/turnos', name: 'app_medico_dashboard_turnos')]
    public function turnos(Request $request, TurnoRepository $turnoRepository, UsuarioRepository $usuarioRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $usuarioRepository->findOneBy(["email"=>$email]);
        $area=$user->getArea();
        return $this->render('medico_dashboard/turnosSinAtender.html.twig', [
            'listTurnos'=>$turnoRepository->getTurnoEstado($area),
        ]);
    }
    #[Route('/medico/dashboard/atender/{id}', name: 'app_medico_dashboard_atender')]
    public function atender(Request $request,Turno $turno, FacturaRepository $facturaRepository,TurnoRepository $turnoRepository,TipoTurnoRepository $tipoTurnoRepository,ManagerRegistry $doctrine, UsuarioRepository $usuarioRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        $arrayTiposTurnos = $doctrine->getRepository("App\Entity\TipoTurno")->findAll();
        $objUsuario = $usuarioRepository->findOneBy(["email"=>$email]);
        $form = $this->createForm(RegisterTurnoType::class, $turno, ['accion'=>'editMedico','arrayTiposTurnos'=>$arrayTiposTurnos]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $turno=$form->getData();
            $turno->setMedicoId($objUsuario->getId());
            $turno->setEstado("Atendido");
           // $turno->setEstado("En Espera");
            $this->addFlash("success", "Exitos: La cita fue creada con exito, cuando su cita sea agendada se le enviara un mensaje");
            $turnoRepository->save($turno,true);
            
            $nuevaFactura=new Factura();
            $nuevaFactura->setFecha(new \Datetime());
            $nuevaFactura->setTurnoId($turno->getId());
            $nuevaFactura->setPacienteId($turno->getPacienteId());
            $precio=$tipoTurnoRepository->findOneBy(['id'=>($turno->getTipoId())])->getPrecio();
            $nuevaFactura->setTotal($precio);
            $nuevaFactura->setEstado("No Pagada");
            $nuevaFactura->setEstadoBase("A");
            //$facturaRep=$doctrine->getRepository("App\Entity\Factura");
            $facturaRepository->save($nuevaFactura,true);
            
        
          return $this->redirectToRoute('app_medico_dashboard');  
        }
        return $this->render('medico_dashboard/editarTurno.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }
}
