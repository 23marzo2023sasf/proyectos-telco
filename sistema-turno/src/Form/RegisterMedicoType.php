<?php

namespace App\Form;

use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegisterMedicoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            
            //->add('roles')
            ->add('nombres', TextType :: class)
            ->add('apellidos', TextType :: class)
            ->add('email', EmailType :: class)
            ->add('password', PasswordType :: class);
                $builder->add('area', ChoiceType:: class, array(
                'choices' => array(
                    'General' => 'General',
                    'Oftalmonologia' => 'Oftalmonologia',
                    'Pediatria' => 'Pediatria',
                    'Traumatologia' =>'Traumatologia',
                    'Oncologia' => 'Oncologia',
                    'Dermatologia' => 'Dermatologia') 
                ));
            
            $builder->add('save', SubmitType :: class, ['label' => 'Registrar'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
        ]);
    }
}
