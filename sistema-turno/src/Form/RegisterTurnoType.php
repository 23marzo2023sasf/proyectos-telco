<?php

namespace App\Form;

use App\Entity\Turno;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class RegisterTurnoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if($options['accion']=='crearTurno')
        {
            $builder
            ->add('fecha', DateTimeType::class,["widget"=>"single_text"])
            ->add('descripcion',TextType::class)
            ->add('tipo_id',ChoiceType:: class, array(
                'choices' => array(
                    'General' => 1,
                    'Oftalmonologia' => 2,
                    'Pediatria' => 3,
                    'Traumatologia' =>4,
                    'Oncologia' => 5,
                    'Dermatologia' => 6)
                ))
                /*->add('tipo_id',ChoiceType::class,
                ['choices'=>$options["arrayTiposTurnos"],//array que se envia desde el controlador
                 'choice_value'=>'id',//atributo de valor que se setea al seleccionar una opcion
                 'choice_label'=>'descripcion',//lo que se mostrara en pantala en las opciones
                 'label'=>'Tipo',//lo que se muestra en pantalla por defecto
                 'mapped'=>false])    */
                ;
        }

           
             if ($options['accion']=='editMedico')
             {
                $builder
                ->add('fecha', DateTimeType::class,["widget"=>"single_text","disabled"=>true])
                ->add('descripcion',TextType::class,["disabled"=>true])
                 ->add('tipo_id',ChoiceType:: class, array(
                     'choices' => array(
                        'General' => 1,
                        'Oftalmonologia' => 2,
                        'Pediatria' => 3,
                        'Traumatologia' =>4,
                        'Oncologia' => 5,
                        'Dermatologia' => 6)
                     ,'disabled'=> true))
                //->add('receta',ArrayT::class)
                ->add('observacion',TextType::class);
             }
             
        $builder->add('save', SubmitType :: class, ['label' => 'Registrar']);
        
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Turno::class,
            'accion' => 'crearTurno',
            'arrayTiposTurnos'=> array()
        ]);
    }
}
