<?php

namespace App\Form;

use App\Entity\Turno;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FiltroFacturaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('fecha', DateTimeType::class,["widget"=>"single_text"])
        ->add('tipo_id',ChoiceType:: class, array(
            'choices' => array(
                'General' => 1,
                'Oftalmonologia' => 2,
                'Pediatria' => 3,
                'Traumatologia' =>4,
                'Oncologia' => 5,
                'Dermatologia' => 6)
            ))
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Turno::class,
        ]);
    }
}
