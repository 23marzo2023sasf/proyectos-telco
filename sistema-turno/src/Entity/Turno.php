<?php

namespace App\Entity;

use App\Repository\TurnoRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TurnoRepository::class)]
class Turno
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $fecha = null;

    #[ORM\Column(length: 100)]
    private ?string $descripcion = null;

    #[ORM\Column]
    private ?int $tipo_id = null;

    #[ORM\Column(length: 25)]
    private ?string $estado = null;

    #[ORM\Column(length: 1, nullable: true)]
    private ?string $estado_base = null;

    #[ORM\Column]
    private ?int $paciente_id = null;

    #[ORM\Column(nullable: true)]
    private ?int $medico_id = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    private array $receta = [];

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $observacion = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getTipoId(): ?int
    {
        return $this->tipo_id;
    }

    public function setTipoId(int $tipo_id): self
    {
        $this->tipo_id = $tipo_id;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getEstadoBase(): ?string
    {
        return $this->estado_base;
    }

    public function setEstadoBase(?string $estado_base): self
    {
        $this->estado_base = $estado_base;

        return $this;
    }

    public function getPacienteId(): ?int
    {
        return $this->paciente_id;
    }

    public function setPacienteId(int $paciente_id): self
    {
        $this->paciente_id = $paciente_id;

        return $this;
    }

    public function getMedicoId(): ?int
    {
        return $this->medico_id;
    }

    public function setMedicoId(?int $medico_id): self
    {
        $this->medico_id = $medico_id;

        return $this;
    }

    public function getReceta(): array
    {
        return $this->receta;
    }

    public function setReceta(?array $receta): self
    {
        $this->receta = $receta;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }
}
