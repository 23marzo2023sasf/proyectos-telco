<?php

namespace App\Repository;

use App\Entity\Factura;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Factura>
 *
 * @method Factura|null find($id, $lockMode = null, $lockVersion = null)
 * @method Factura|null findOneBy(array $criteria, array $orderBy = null)
 * @method Factura[]    findAll()
 * @method Factura[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacturaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Factura::class);
    }

    public function save(Factura $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Factura $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function getFacturaCajero(): ?array
    {
        //se hace un left join para obtener las turnos aun cuando no tengan registrado un medico Id
        $strSql = "SELECT facturas.id,
                   facturas.fecha,
                   facturas.total,
                   facturas.estado,
                   turnos.descripcion descripcion,
                   userPaciente.nombres paci_nombres,
                   userPaciente.apellidos paci_apellidos
                   FROM App\Entity\Factura facturas
                   JOIN App\Entity\Usuario userPaciente
                   WITH facturas.paciente_id = userPaciente.id
                   LEFT JOIN App\Entity\Turno turnos
                   WITH facturas.turno_id = turnos.id
                   WHERE facturas.estado =:estado AND facturas.estado_base = :estadob";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado',"No Pagada")
                    ->setParameter('estadob',"A")
                    ->getResult();         
    }
    public function getFacturaEspecialidad($idTipo): ?array
    {
        //se hace un left join para obtener las turnos aun cuando no tengan registrado un medico Id
        $strSql = "SELECT facturas.id,
                   facturas.fecha,
                   facturas.total,
                   facturas.estado,
                   turnos.descripcion descripcion,
                   userPaciente.nombres paci_nombres,
                   userPaciente.apellidos paci_apellidos
                   FROM App\Entity\Factura facturas
                   JOIN App\Entity\Usuario userPaciente
                   WITH facturas.paciente_id = userPaciente.id
                   LEFT JOIN App\Entity\Turno turnos
                   WITH facturas.turno_id = turnos.id
                   WHERE facturas.estado =:estado AND turnos.tipo_id = :id AND facturas.estado_base = :estadob";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado',"No Pagada")
                    ->setParameter('estadob',"A")
                    ->setParameter('id',$idTipo)
                    ->getResult();         
    }
    public function getFacturaAdmin(): ?array
    {
        //se hace un left join para obtener las turnos aun cuando no tengan registrado un medico Id
        $strSql = "SELECT facturas.id,
                   facturas.fecha,
                   facturas.total,
                   facturas.estado,
                   turnos.descripcion descripcion,
                   userCajero.nombres cajero_nombres,
                   userCajero.apellidos cajero_apellidos,
                   userPaciente.nombres paci_nombres,
                   userPaciente.apellidos paci_apellidos
                   FROM App\Entity\Factura facturas
                   JOIN App\Entity\Usuario userCajero
                   WITH facturas.cajero_id = userCajero.id
                   JOIN App\Entity\Usuario userPaciente
                   WITH facturas.paciente_id = userPaciente.id
                   LEFT JOIN App\Entity\Turno turnos
                   WITH facturas.turno_id = turnos.id
                   WHERE facturas.estado =:estado AND facturas.estado_base = :estadob ";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado',"Pagada")
                    ->setParameter('estadob',"A")
                    ->getResult();         
    }

//    /**
//     * @return Factura[] Returns an array of Factura objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Factura
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
