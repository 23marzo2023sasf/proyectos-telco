CREATE DATABASE db_sistema_Turnos
DROP TABLE IF EXISTS medicamento;
DROP TABLE IF EXISTS factura;
DROP TABLE IF EXISTS turno;
DROP TABLE IF EXISTS tipoTurno;
DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario(
id BIGINT UNSIGNED AUTO_INCREMENT,
email VARCHAR(100) UNIQUE NOT NULL,
password VARCHAR(200) NOT NULL,
roles  array() NOT NULL,
nombres VARCHAR(50) NOT NULL,
apellidos VARCHAR(50) NOT NULL,
tipo VARCHAR(50) NOT NULL,
estado_base VARCHAR(1) ,
CONSTRAINT PK_usuario PRIMARY KEY (id)
);

CREATE TABLE medicamento(
    id BIGINT UNSIGNED AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    descripcion VARCHAR(100) NOT NULL,
    precio FLOAT NOT NULL,
    estado_base VARCHAR(1),
    CONSTRAINT PK_medicamento PRIMARY KEY (id)
);

CREATE TABLE tipoTurno(
    id BIGINT UNSIGNED AUTO_INCREMENT,
    descripcion VARCHAR(100) NOT NULL,
    precio FLOAT NOT NULL,
    estado_base VARCHAR(1),
    CONSTRAINT PK_tipoTurno PRIMARY KEY (id)
);

CREATE TABLE turno(
id BIGINT UNSIGNED AUTO_INCREMENT,
total FLOAT NOT NULL,
estado varchar(200) NOT NULL,
cajero_id INT NOT NULL,
paciente_id INT NOT NULL,
turno_id INT NOT NULL,
estado_base VARCHAR(1) NOT NULL,
CONSTRAINT PK_turno PRIMARY KEY (id),
CONSTRAINT FK_cajero_factura FOREIGN KEY (cajero_id) REFERENCES dbo.usuario (id),
CONSTRAINT FK_paciente_factura FOREIGN KEY (paciente_id) REFERENCES dbo.usuario (id),
CONSTRAINT FK_turno_factura FOREIGN KEY (turno_id) REFERENCES dbo.turno (id),
  
);

CREATE TABLE factura(
id BIGINT UNSIGNED AUTO_INCREMENT,
fecha
total FLOAT NOT NULL,
estado varchar(200) NOT NULL,
cajero_id INT NOT NULL,
paciente_id INT NOT NULL,
turno_id INT NOT NULL,
estado_base VARCHAR(1) NOT NULL,
CONSTRAINT PK_factura PRIMARY KEY (id),
CONSTRAINT FK_cajero_factura FOREIGN KEY (cajero_id) REFERENCES dbo.usuario (id),
CONSTRAINT FK_paciente_factura FOREIGN KEY (paciente_id) REFERENCES dbo.usuario (id),
CONSTRAINT FK_turno_factura FOREIGN KEY (turno_id) REFERENCES dbo.turno (id),
  
);