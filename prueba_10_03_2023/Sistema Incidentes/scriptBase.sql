DROP TABLE IF EXISTS usuario;
USE db_prueba;
CREATE TABLE usuario( 
    id BIGINT PRIMARY KEY AUTO_INCREMENT, 
    email VARCHAR(100) UNIQUE NOT NULL, 
    password VARCHAR(1) NOT NULL,
    roles JSON NOT NULL, 
    nombres VARCHAR(50) NOT NULL, 
    apellidos VARCHAR(50) NOT NULL, 
    tipo VARCHAR(25) NOT NULL, estado VARCHAR(1) );
    