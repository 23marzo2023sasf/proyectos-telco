DROP TABLE IF EXISTS paquete;
DROP TABLE IF EXIsTS ruta;
DROP TABLE IF EXISTS destino
DROP TABLE IF EXISTS usuario;
USE db_prueba;
CREATE TABLE usuario( 
    id BIGINT PRIMARY KEY AUTO_INCREMENT, 
    email VARCHAR(100) UNIQUE NOT NULL, 
    password VARCHAR(1) NOT NULL,
    roles JSON NOT NULL, 
    nombres VARCHAR(50) NOT NULL, 
    apellidos VARCHAR(50) NOT NULL, 
    tipo VARCHAR(25) NOT NULL, estado VARCHAR(1) );

CREATE TABLE destino (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(100) NOT NULL,
    estado_base VARCHAR(1) NOT NULL
);
CREATE TABLE ruta (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    repartidor_id BIGINT FOREIGN KEY NOT NULL,
    destinos array NOT NULL,
    estado_base VARCHAR(1) NOT NULL
);
CREATE TABLE paquete(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    destino VARCHAR(100) NOT NULL,
    estado VARCHAR(20) NOT NULL,
    estado_base VARCHAR(1) NOT NULL,
    secretaria_id BIGINT  FOREIGN KEY NOT NULL,
    repartidot_id BIGINT FOREIGN KEY,
    tipo VARCHAR(20) NOT NULL,
    emisor VARCHAR(25) NOT NULL,
    emisor_id VARCHAR(20) NOT NULL,
    receptor VARCHAR(25) NOT NULL,
    receptor_id VARCHAR(20) NOT NULL,
); 