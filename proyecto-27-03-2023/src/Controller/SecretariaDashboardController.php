<?php

namespace App\Controller;

use App\Entity\Ruta;
use App\Entity\Paquete;
use App\Form\RegisterRutaType;
use App\Form\RegisterPaqueteType;
use App\Repository\RutaRepository;
use App\Repository\PaqueteRepository;
use App\Repository\UsuarioRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SecretariaDashboardController extends AbstractController
{
    #[Route('/secretaria/dashboard', name: 'app_secretaria_dashboard')]
    public function index(Request $request, PaqueteRepository $paqueteRepository, UsuarioRepository $UsuarioRepository): Response
    {
        //$user = $request ->getUser();
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $UsuarioRepository->findOneBy(["email"=>$email]);
        return $this->render('secretaria_dashboard/index.html.twig', [
            'listPaquetes'=>$paqueteRepository->getPaqueteSecretaria($user->getId()),
        ]);
    }
    #[Route('/secretaria/dashboard/register', name: 'app_secretaria_dashboard_register')]
    public function agregar(Request $request, PaqueteRepository $paqueteRepository,ManagerRegistry $doctrine, UsuarioRepository $UsuarioRepository): Response
    {
        //$user = $request->request ->ge()
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $UsuarioRepository->findOneBy(["email"=>$email]);
        $paquete=new Paquete();
        $form =$this->createForm(RegisterPaqueteType::class, $paquete);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $paquete=$form->getData();
            $paquete->setSecretariaId($user->getId());
            $paquete->setEstadoBase("A"); 
            $em=$doctrine->getManager();
            $em->persist($paquete);
            $em->flush();
            $this->addFlash("success","Registro de Paquete Exitoso");
            return $this->redirectToRoute('app_secretaria_dashboard');
        }
        return $this->render('secretaria_dashboard/registerPaquete.html.twig', [
            'formulario'=>$form->createView(),
        ]);
    }

    #[Route('/secretaria/dashboard/edit/{id}', name: 'app_secretaria_dashboard_edit')]
    public function edit(Request $request,Paquete $paquete, PaqueteRepository $paqueteRepository,ManagerRegistry $doctrine, UsuarioRepository $UsuarioRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        
        $objUsuario = $UsuarioRepository->findOneBy(["email"=>$email]);
        
        $form = $this->createForm(RegisterPaqueteType::class, $paquete);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if($paquete->getEstado() == "No Entregado")    
        {
            $paquete=$form->getData();
            $paquete->setsecretariaId($objUsuario->getId());
           
            $this->addFlash("success", "Exitos: El paquete fue editado con exito");
            $paqueteRepository->save($paquete,true);
            
        }
        else 
        {
            $this->addFlash("Error", "Error: No puede eliminar un paquete que ya se ha entregado"); 
        }
          return $this->redirectToRoute('app_secretaria_dashboard');  
        }
        return $this->render('secretaria_dashboard/registerTurno.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }

    #[Route('/secretaria/dashboard/delete/{id}', name: 'app_secretaria_dashboard_delete')]
    public function delete(Paquete $paquete, PaqueteRepository $paqueteRepository): Response
    {
       
            $paquete-> setEstadoBase("I"); 
            $paqueteRepository->save($paquete,true);
        return $this->redirectToRoute('app_secretaria_dashboard');
    }
    #[Route('/secretaria/dashboard/crearRuta', name: 'app_secretaria_dashboard_crearRuta')]
    public function crearRuta(Request $request, PaqueteRepository $paqueteRepository,RutaRepository $rutaRepository, UsuarioRepository $UsuarioRepository): Response
    {
        
        $ruta= new Ruta();
        $ruta->setDestinos(array());
        $listaPaquetes=$paqueteRepository->getPaquete();
        $form = $this->createForm(RegisterRutaType::class, $ruta);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {    
        
            $ruta=$form->getData();
            $ruta->setEstado("En Espera");
            $ruta->setEstadoBase("A");
            $this->addFlash("success", "Exitos: El paquete fue editado con exito");
            $rutaRepository->save($ruta,true);
            
       
          return $this->redirectToRoute('app_repartidor_dashboard');  
        }
        return $this->render('secretaria_dashboard/paquetes.html.twig', [
            'formulario'=>$form->createView(),
            
        ]);
    }
    


}
