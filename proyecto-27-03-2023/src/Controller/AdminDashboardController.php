<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Repository\PaqueteRepository;
use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminDashboardController extends AbstractController
{
    #[Route('/admin/dashboard', name: 'app_admin_dashboard')]
    public function index(Request $request, UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('admin_dashboard/index.html.twig', [
            'listUsuarios' => $usuarioRepository->findAll(),
        ]);
    }
    #[Route('/admin/dashboard/paquetes', name: 'app_admin_dashboard_paquetes')]
    public function paquetes(Request $request,PaqueteRepository $paqueteRepository, UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('admin_dashboard/paquetes.html.twig', [
            'listPaquetes' => $paqueteRepository->getPaqueteAdmin(),
        ]);
    }
}
