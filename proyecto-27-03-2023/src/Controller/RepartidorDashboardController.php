<?php

namespace App\Controller;

use App\Entity\Paquete;
use App\Repository\PaqueteRepository;
use App\Repository\UsuarioRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RepartidorDashboardController extends AbstractController
{
    #[Route('/repartidor/dashboard', name: 'app_repartidor_dashboard')]
    public function index(Request $request, PaqueteRepository $paqueteRepository, UsuarioRepository $UsuarioRepository): Response
    {
        //$user = $request ->getUser();
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $UsuarioRepository->findOneBy(["email"=>$email]);
        return $this->render('repartidor_dashboard/index.html.twig', [
            'listPaquetes'=>$paqueteRepository->getPaqueteEstado($user->getId()),
        ]);
    }
    #[Route('/repartidor/dashboard/edit/{id}', name: 'app_repartidor_dashboard_edit')]
    public function edit(Request $request,Paquete $paquete, PaqueteRepository $paqueteRepository,ManagerRegistry $doctrine, UsuarioRepository $UsuarioRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        
        $objUsuario = $UsuarioRepository->findOneBy(["email"=>$email]);
        
        $form = $this->createForm(RegisterPaqueteType::class, $paquete,['accion'=>'editarPaquete']);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {    
        
            $paquete=$form->getData();
            $paquete->setrepartidorId($objUsuario->getId());
           
            $this->addFlash("success", "Exitos: El paquete fue editado con exito");
            $paqueteRepository->save($paquete,true);
            
       
          return $this->redirectToRoute('app_repartidor_dashboard');  
        }
        return $this->render('repartidor_dashboard/registerTurno.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }
}
