<?php

namespace App\Repository;

use App\Entity\Paquete;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Paquete>
 *
 * @method Paquete|null find($id, $lockMode = null, $lockVersion = null)
 * @method Paquete|null findOneBy(array $criteria, array $orderBy = null)
 * @method Paquete[]    findAll()
 * @method Paquete[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaqueteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Paquete::class);
    }

    public function save(Paquete $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Paquete $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function getPaqueteSecretaria($idUser): ?array
    {
        //se hace un left join para obtener las paquetes aun cuando no tengan registrado un medico Id
        $strSql = "SELECT paquetes.id,
                   paquetes.peso,
                   paquetes.destino,
                   paquetes.estado,
                   paquetes.tipo
                   FROM App\Entity\Paquete paquetes
                   JOIN App\Entity\Usuario userSecretaria
                   WITH paquetes.secretaria_id = userSecretaria.id
                   WHERE paquetes.secretaria_id =:secretaria AND paquetes.estado_base = :estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('secretaria',$idUser)
                    ->setParameter('estado',"A")
                    ->getResult();         
    }
    public function getPaquete(): ?array
    {
        //se hace un left join para obtener las paquetes aun cuando no tengan registrado un medico Id
        $strSql = "SELECT paquetes.id,
                   paquetes.peso,
                   paquetes.destino,
                   paquetes.estado,
                   paquetes.tipo
                   FROM App\Entity\Paquete paquetes
                   JOIN App\Entity\Usuario userSecretaria
                   WITH paquetes.secretaria_id = userSecretaria.id
                   WHERE paquetes.estado =:estad AND paquetes.estado_base = :estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estad','No Entregado')
                    ->setParameter('estado',"A")
                    ->getResult();         
    }
    public function getPaqueteEstado($idUser): ?array
    {
        //se hace un left join para obtener las paquetes aun cuando no tengan registrado un medico Id
        $strSql = "SELECT paquetes.id,
                   paquetes.peso,
                   paquetes.destino,
                   paquetes.estado,
                   paquetes.tipo,
                   userSecretaria.nombres secretaria_nombres,
                   userSecretaria.apellidos secretaria_apellidos
                   FROM App\Entity\Paquete paquetes
                   JOIN App\Entity\Usuario userSecretaria
                   WITH paquetes.secretaria_id = userSecretaria.id
                   WHERE paquetes.repartidor_id =:repartidor AND paquetes.estado_base = :estado";
        return $this->_em->createQuery($strSql)
                    ->setParameter('repartidor',$idUser)
                    ->setParameter('estado',"A")
                    ->getResult();         
    }
    public function getPaqueteAdmin(): ?array
    {
        //se hace un left join para obtener las paquetes aun cuando no tengan registrado un medico Id
        $strSql = "SELECT paquetes.id,
                   paquetes.peso,
                   paquetes.destino,
                   paquetes.estado,
                   paquetes.tipo,
                   userSecretaria.nombres secretaria_nombres,
                   userSecretaria.apellidos secretaria_apellidos,
                   userRepartidor.nombres repartidor_nombres,
                   userRepartidor.apellidos repartidor_apellidos
                   FROM App\Entity\Paquete paquetes
                   JOIN App\Entity\Usuario userRepartidor
                   WITH paquetes.repartidor_id = userRepartidor.id
                   LEFT JOIN App\Entity\Usuario userSecretaria
                   WITH paquetes.secretaria_id = userSecretaria.id
                   WHERE paquetes.estado =:estado  AND paquetes.estado_base =:estadob";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado','Entregado')
                    ->setParameter('estadob',"A")
                    ->getResult();         
    }

//    /**
//     * @return Paquete[] Returns an array of Paquete objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Paquete
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
