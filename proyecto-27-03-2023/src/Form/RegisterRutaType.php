<?php

namespace App\Form;

use App\Entity\Ruta;
use App\Entity\Paquete;
use App\Repository\PaqueteRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterRutaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('repartidor_id',ChoiceType:: class, array(
                'choices' => array(
                    'Juan Daniel' => 2,
                    'Oscar Alberto' => 3,
                    'Rafael Steven' => 4)
                
                ))
                ->add('paquetes', EntityType::class, [
                    'class' => Paquete::class,
                    'choices' => $options['lista'],
                    'choice_label' => 'username',
                ])
                ->add('save', SubmitType :: class, ['label' => 'Guardar']);
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ruta::class,
            'lista'=> array()
        ]);
    }
}
