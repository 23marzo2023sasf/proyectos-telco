<?php

namespace App\Form;

use App\Entity\Paquete;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterPaqueteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if($options['accion']=='crearPaquete')
        {
            $builder
            ->add('peso', NumberType::class)
            ->add('destino',TextType::class)
            
            //->add('estado_base')
            //->add('secretaria_id')
            //->add('repartidor_id')
            ->add('tipo',ChoiceType:: class, array(
                'choices' => array(
                    'Fragil' => 'Fragil',
                    'Pesado' => 'Pesado',
                    'Normal' => 'Normal')
                ))
            ->add('emisor',TextType::class)
            ->add('emisor_id',TextType::class)
            ->add('receptor',TextType::class)
            ->add('receptor_id',TextType::class)
            ->add('estado',ChoiceType:: class, array(
                'choices' => array(
                    'No Entregado' => 'No Entregado',
                    'Entregado' => 'Entregado',
                    'Cancelado' => 'Cancelado')
                
                ));
        

        }
        if($options['accion']=='editarPaquete')
        {
            $builder
            ->add('peso', NumberType::class,["disbaled"=>true])
            ->add('destino',TextType::class,["disbaled"=>true])
            //->add('estado')
            //->add('estado_base')
            //->add('secretaria_id')
            //->add('repartidor_id')
            ->add('tipo',ChoiceType:: class, array(
                'choices' => array(
                    'Fragil' => 'Fragil',
                    'Pesado' => 'Pesado',
                    'Normal' => 'Normal'),
                    'disabled'=>true
                
                ))
            ->add('emisor',TextType::class,["disbaled"=>true])
            ->add('emisor_id',TextType::class,["disbaled"=>true])
            ->add('receptor',TextType::class,["disbaled"=>true])
            ->add('receptor_id',TextType::class,["disbaled"=>true])
             ->add('estado',ChoiceType:: class, array(
                'choices' => array(
                    'No Entregado' => 'No Entregado',
                    'Entregado' => 'Entregado')
                
                ));
        }
        $builder->add('save', SubmitType :: class, ['label' => 'Guardar']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Paquete::class,
            'accion' => 'crearPaquete'
        ]);
    }
}
