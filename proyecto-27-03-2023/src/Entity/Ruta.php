<?php

namespace App\Entity;

use App\Repository\RutaRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RutaRepository::class)]
class Ruta
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $repartidor_id = null;

    #[ORM\Column(type: Types::ARRAY)]
    private array $destinos = [];

    #[ORM\Column(length: 1)]
    private ?string $estado_base = null;

    #[ORM\Column(length: 20)]
    private ?string $estado = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRepartidorId(): ?int
    {
        return $this->repartidor_id;
    }

    public function setRepartidorId(int $repartidor_id): self
    {
        $this->repartidor_id = $repartidor_id;

        return $this;
    }

    public function getDestinos(): array
    {
        return $this->destinos;
    }

    public function setDestinos(array $destinos): self
    {
        $this->destinos = $destinos;

        return $this;
    }

    public function getEstadoBase(): ?string
    {
        return $this->estado_base;
    }

    public function setEstadoBase(string $estado_base): self
    {
        $this->estado_base = $estado_base;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }
}
