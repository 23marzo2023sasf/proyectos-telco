<?php

namespace App\Entity;

use App\Repository\PaqueteRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PaqueteRepository::class)]
class Paquete
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $peso = null;

    #[ORM\Column(length: 100)]
    private ?string $destino = null;

    #[ORM\Column(length: 25)]
    private ?string $estado = null;

    #[ORM\Column(length: 1)]
    private ?string $estado_base = null;

    #[ORM\Column]
    private ?int $secretaria_id = null;

    #[ORM\Column(nullable: true)]
    private ?int $repartidor_id = null;

    #[ORM\Column(length: 20)]
    private ?string $tipo = null;

    #[ORM\Column(length: 25)]
    private ?string $emisor = null;

    #[ORM\Column(length: 20)]
    private ?string $emisor_id = null;

    #[ORM\Column(length: 25)]
    private ?string $receptor = null;

    #[ORM\Column(length: 20)]
    private ?string $receptor_id = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPeso(): ?int
    {
        return $this->peso;
    }

    public function setPeso(int $peso): self
    {
        $this->peso = $peso;

        return $this;
    }

    public function getDestino(): ?string
    {
        return $this->destino;
    }

    public function setDestino(string $destino): self
    {
        $this->destino = $destino;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getEstadoBase(): ?string
    {
        return $this->estado_base;
    }

    public function setEstadoBase(string $estado_base): self
    {
        $this->estado_base = $estado_base;

        return $this;
    }

    public function getSecretariaId(): ?int
    {
        return $this->secretaria_id;
    }

    public function setSecretariaId(int $secretaria_id): self
    {
        $this->secretaria_id = $secretaria_id;

        return $this;
    }

    public function getRepartidorId(): ?int
    {
        return $this->repartidor_id;
    }

    public function setRepartidorId(?int $repartidor_id): self
    {
        $this->repartidor_id = $repartidor_id;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getEmisor(): ?string
    {
        return $this->emisor;
    }

    public function setEmisor(string $emisor): self
    {
        $this->emisor = $emisor;

        return $this;
    }

    public function getEmisorId(): ?string
    {
        return $this->emisor_id;
    }

    public function setEmisorId(string $emisor_id): self
    {
        $this->emisor_id = $emisor_id;

        return $this;
    }

    public function getReceptor(): ?string
    {
        return $this->receptor;
    }

    public function setReceptor(string $receptor): self
    {
        $this->receptor = $receptor;

        return $this;
    }

    public function getReceptorId(): ?string
    {
        return $this->receptor_id;
    }

    public function setReceptorId(string $receptor_id): self
    {
        $this->receptor_id = $receptor_id;

        return $this;
    }
}
