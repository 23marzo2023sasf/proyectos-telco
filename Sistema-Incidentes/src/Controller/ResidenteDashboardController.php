<?php

namespace App\Controller;

use App\Entity\Incidente;
use App\Form\RegisterIncidenteType;
use App\Repository\UsuarioRepository;
use App\Repository\IncidenteRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\TipoIncidenteRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ResidenteDashboardController extends AbstractController
{
    #[Route('/residente/dashboard', name: 'app_residente_dashboard')]
    public function index(Request $request, IncidenteRepository $IncidenteRepository, UsuarioRepository $usuarioRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $usuarioRepository->findOneBy(["email"=>$email]);
        return $this->render('residente_dashboard/index.html.twig', [
            'listIncidentes'=>$IncidenteRepository->getIncidenteResidente($user->getId()),
        ]);
    }

#[Route('/residente/dashboard/register', name: 'app_residente_dashboard_register')]
    public function agregar(Request $request,TipoIncidenteRepository $tipoIncidenteRepository,IncidenteRepository $incidenteRepository,ManagerRegistry $doctrine, UsuarioRepository $usuarioRepository): Response
    {
        //$user = $request->request ->ge()
        $email = $request->getSession()->get('_security.last_username', '');
        
        $user = $usuarioRepository->findOneBy(["email"=>$email]);
        $arrayTipos = $tipoIncidenteRepository->findAll();
        $incidente=new Incidente();
        $form =$this->createForm(RegisterIncidenteType::class, $incidente, ['arrayTipos'=>$arrayTipos]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $incidente=$form->getData();
            $incidente->setResidenteId($user->getId());
            $incidente->setEstado("En Espera");
            $incidente->setEstadoBase("A");
            //$incidente->setDuracion(30);
            //$incidente->setTipoId(1);
            $em=$doctrine->getManager();
            $em->persist($incidente);
            $em->flush();
            $this->addFlash("success","Registro de Incidente Exitoso");
            return $this->redirectToRoute('app_residente_dashboard');
        }
        return $this->render('residente_dashboard/registerIncidente.html.twig', [
            'formulario'=>$form->createView(),
        ]);
    }
    #[Route('/residente/dashboard/edit/{id}', name: 'app_residente_dashboard_edit')]
    public function edit(Request $request,Incidente $incidente,TipoIncidenteRepository $tipoIncidenteRepository ,IncidenteRepository $incidenteRepository,ManagerRegistry $doctrine, UsuarioRepository $usuarioRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        
        $objUsuario = $usuarioRepository->findOneBy(["email"=>$email]);
        $arrayTipos = $tipoIncidenteRepository->findAll();
        $form =$this->createForm(RegisterIncidenteType::class, $incidente, ['arrayTipos'=>$arrayTipos]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if($incidente->getEstado() == "En Espera")    
        {
            $incidente=$form->getData();
            $incidente->setResidenteId($objUsuario->getId());
           // $incidente->setEstado("En Espera");
            $this->addFlash("success", "Exitos: El Incidente fue editado con exito");
            $incidenteRepository->save($incidente,true);
            
        }
        else 
        {
            $this->addFlash("Error", "Error: No puede eliminar un incidente que ya ha sido atendido"); 
        }
          return $this->redirectToRoute('app_residente_dashboard');  
        }
        return $this->render('residente_dashboard/registerIncidente.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }
    #[Route('/residente/dashboard/delete/{id}', name: 'app_residente_dashboard_delete')]
    public function delete(Incidente $incidente, IncidenteRepository $incidenteRep): Response
    {
        if($incidente->getEstado() == "En Espera")    
        {
            $incidente->setEstadoBase("I");
            $this->addFlash("success", "Incidente eliminado con exito");
            
        }
        else 
        {
            $this->addFlash("error", "Error: No puede eliminar un incidente que no haya sido atendido"); 
        }
        
        return $this->redirectToRoute('app_residente_dashboard');
    }
}