<?php

namespace App\Entity;

use App\Repository\TipoIncidenteRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TipoIncidenteRepository::class)]
class TipoIncidente
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $descripcion = null;

    #[ORM\Column(length: 1, nullable: true)]
    private ?string $estado_base = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEstadoBase(): ?string
    {
        return $this->estado_base;
    }

    public function setEstadoBase(?string $estado_base): self
    {
        $this->estado_base = $estado_base;

        return $this;
    }
}
